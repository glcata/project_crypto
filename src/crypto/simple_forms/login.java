/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crypto.simple_forms;

import crypto.encrypt.encrypt;
import crypto.helper.dragDrop_listener;
import crypto.helper.configApp;
import de.javasoft.plaf.synthetica.SyntheticaBlackEyeLookAndFeel;
import java.awt.Point;
import java.awt.dnd.DropTarget;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.UnsupportedLookAndFeelException;
import crypto.helper.helper;
import crypto.sql.DBSelect;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Locale;
import java.util.ResourceBundle;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author Catalin Glavan
 */
public class login extends javax.swing.JFrame implements helper {

    public login(Point location, ResourceBundle bundle) {
        lang = bundle.getBaseBundleName().substring(bundle.getBaseBundleName().length() - 2);
        this.bundle = bundle;
        initComponents();
        super.setIconImage(new ImageIcon(getClass().getResource(config.getIconPath())).getImage());
        jsign_in.requestFocus();

        checkLocation(location);
        checkRememberMe();
        closeWindow();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelUP = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        back = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jname = new javax.swing.JLabel();
        name_txt = new javax.swing.JTextField();
        jemail = new javax.swing.JLabel();
        email_txt = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        key_store = new javax.swing.JButton();
        key = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jremember = new javax.swing.JLabel();
        check_remember = new javax.swing.JCheckBox();
        jsign_up = new javax.swing.JButton();
        jsign = new javax.swing.JLabel();
        jsign_in = new javax.swing.JButton();
        jPanelDOWN = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle(bundle.getString("key_login_title")
        );
        setResizable(false);
        getContentPane().setLayout(new javax.swing.BoxLayout(getContentPane(), javax.swing.BoxLayout.PAGE_AXIS));

        jPanelUP.addAncestorListener(new javax.swing.event.AncestorListener() {
            public void ancestorMoved(javax.swing.event.AncestorEvent evt) {
                jPanelUPAncestorMoved(evt);
            }
            public void ancestorAdded(javax.swing.event.AncestorEvent evt) {
            }
            public void ancestorRemoved(javax.swing.event.AncestorEvent evt) {
            }
        });

        jLabel1.setFont(new java.awt.Font("Dialog", 1, 16)); // NOI18N
        jLabel1.setText(bundle.getString("key_log"));

        back.setIcon(new javax.swing.ImageIcon(getClass().getResource("/crypto/images/back_off.png"))); // NOI18N
        back.setBorder(null);
        back.setFocusable(false);
        back.setBorderPainted(false);
        back.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backActionPerformed(evt);
            }
        });

        jname.setFont(new java.awt.Font("Dialog", 1, 13)); // NOI18N
        jname.setText(bundle.getString("key_user"));

        name_txt.setColumns(14);
        name_txt.setFont(new java.awt.Font("Dialog", 0, 13)); // NOI18N

        jemail.setFont(new java.awt.Font("Dialog", 1, 13)); // NOI18N
        jemail.setText(bundle.getString("key_email"));

        email_txt.setColumns(14);
        email_txt.setFont(new java.awt.Font("Dialog", 0, 13)); // NOI18N

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/crypto/images/simple_forms/name_user.png"))); // NOI18N

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/crypto/images/simple_forms/email.png"))); // NOI18N

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(30, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jemail)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(email_txt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jname)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(name_txt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(27, 27, 27))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(11, 11, 11)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jname)
                            .addComponent(name_txt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 12, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jemail)
                            .addComponent(email_txt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(13, 13, 13))))
        );

        key_store.setIcon(new javax.swing.ImageIcon(getClass().getResource("/crypto/images/simple_forms/inputs.png"))); // NOI18N
        key_store.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                key_storeActionPerformed(evt);
            }
        });
        dragDrop_listener drag_and_drop = new dragDrop_listener(this);
        new DropTarget(key_store, drag_and_drop);

        key.setIcon(new javax.swing.ImageIcon(getClass().getResource("/crypto/images/simple_forms/key.png"))); // NOI18N

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap(114, Short.MAX_VALUE)
                .addComponent(key)
                .addGap(18, 18, 18)
                .addComponent(key_store)
                .addGap(80, 80, 80))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(key, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(key_store))
                .addContainerGap(15, Short.MAX_VALUE))
        );

        jremember.setFont(new java.awt.Font("Dialog", 1, 13)); // NOI18N
        jremember.setText(bundle.getString("key_rem"));
        jremember.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jrememberMouseClicked(evt);
            }
        });

        check_remember.setFont(new java.awt.Font("Dialog", 1, 13)); // NOI18N
        check_remember.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                check_rememberActionPerformed(evt);
            }
        });

        jsign_up.setFont(new java.awt.Font("Dialog", 1, 13)); // NOI18N
        jsign_up.setForeground(new java.awt.Color(0, 102, 255));
        jsign_up.setText(bundle.getString("key_help2"));
        jsign_up.setBorder(null);
        jsign_up.setBorderPainted(false);
        jsign_up.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jsign_upActionPerformed(evt);
            }
        });

        jsign.setFont(new java.awt.Font("Dialog", 1, 13)); // NOI18N
        jsign.setText(bundle.getString("key_help"));

        jsign_in.setFont(new java.awt.Font("Dialog", 1, 13)); // NOI18N
        jsign_in.setIcon(new javax.swing.ImageIcon(getClass().getResource("/crypto/images/simple_forms/sign_in.png"))); // NOI18N
        jsign_in.setText(bundle.getString("key_sign"));
        jsign_in.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jsign_inActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                        .addComponent(jsign_in, javax.swing.GroupLayout.PREFERRED_SIZE, 217, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(63, 63, 63))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                        .addComponent(jsign)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jsign_up)
                        .addGap(45, 45, 45))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                        .addComponent(check_remember)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jremember)
                        .addGap(119, 119, 119))))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jremember, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(check_remember, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jsign_in, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jsign)
                    .addComponent(jsign_up))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanelUPLayout = new javax.swing.GroupLayout(jPanelUP);
        jPanelUP.setLayout(jPanelUPLayout);
        jPanelUPLayout.setHorizontalGroup(
            jPanelUPLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelUPLayout.createSequentialGroup()
                .addGroup(jPanelUPLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelUPLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanelUPLayout.createSequentialGroup()
                        .addGap(35, 35, 35)
                        .addComponent(back)
                        .addGap(98, 98, 98)
                        .addComponent(jLabel1))
                    .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanelUPLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanelUPLayout.setVerticalGroup(
            jPanelUPLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelUPLayout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addGroup(jPanelUPLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(back, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        getContentPane().add(jPanelUP);

        javax.swing.GroupLayout jPanelDOWNLayout = new javax.swing.GroupLayout(jPanelDOWN);
        jPanelDOWN.setLayout(jPanelDOWNLayout);
        jPanelDOWNLayout.setHorizontalGroup(
            jPanelDOWNLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 380, Short.MAX_VALUE)
        );
        jPanelDOWNLayout.setVerticalGroup(
            jPanelDOWNLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 30, Short.MAX_VALUE)
        );

        getContentPane().add(jPanelDOWN);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void backActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backActionPerformed
        if (evt.getSource() == back) {
            super.dispose();
            new intro(setPoint, lang).setVisible(true);
        }
    }//GEN-LAST:event_backActionPerformed

    private void key_storeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_key_storeActionPerformed
        if (evt.getSource() == key_store) {
            JFileChooser chooser = new JFileChooser();
            FileNameExtensionFilter filter = new FileNameExtensionFilter("Certificate File .crt", "crt");
            chooser.setCurrentDirectory(new java.io.File("user.dir"));
            chooser.setDialogTitle("Choose Digital Certificate");
            chooser.setFileFilter(filter);
            chooser.setAcceptAllFileFilterUsed(true);

            if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                location_key = chooser.getSelectedFile().getAbsolutePath();
                String check_crt = location_key.substring(location_key.length() - 3);
                if (check_crt.equals("crt")) {
                    key_store.setIcon(new javax.swing.ImageIcon(getClass().getResource("/crypto/images/simple_forms/id_verified.png")));
                } else {
                    location_key = null;
                    key_store.setIcon(new javax.swing.ImageIcon(getClass().getResource("/crypto/images/simple_forms/id_not_verified.png")));
                }
            }
        }
    }//GEN-LAST:event_key_storeActionPerformed

    private void jsign_upActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jsign_upActionPerformed
        if (evt.getSource() == jsign_up) {
            new acc(setPoint, bundle).setVisible(true);
            super.dispose();
        }
    }//GEN-LAST:event_jsign_upActionPerformed

    private void jsign_inActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jsign_inActionPerformed
        if (evt.getSource() == jsign_in) {
            encrypt encryptObj = new encrypt(name_txt.getText(), email_txt.getText(), location_key);
            String decryptString = encryptObj.getDecrypt();
            String[] stringSplit = decryptString.split(":");
            ImageIcon icon = new ImageIcon(getClass().getResource(config.getIconPath()));
            if (stringSplit[0].equals(name_txt.getText()) && stringSplit[1].equals(email_txt.getText())) {
                System.out.println(decryptString);
                JOptionPane.showMessageDialog(this, bundle.getString("key_request"), "INFO", JOptionPane.INFORMATION_MESSAGE, icon);

                super.dispose();
                String email = email_txt.getText().trim();
                DBSelect sqlData = new DBSelect();
                sqlData.selectDATA(email);
                new codePanel(email, location_key, sqlData.getCaptcha(), bundle, setPoint).setVisible(true);
                
            } else {
                JOptionPane.showMessageDialog(this, bundle.getString("key_request_failed"), "INFO", JOptionPane.INFORMATION_MESSAGE, icon);
            }
        }
    }//GEN-LAST:event_jsign_inActionPerformed

    private void jrememberMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jrememberMouseClicked
        if (evt.getSource() == jremember) {
            if (!check_remember.isSelected()) {
                check_remember.setSelected(true);
                setRememberMe();
            } else {
                check_remember.setSelected(false);
                setRememberMe();
            }
        }
    }//GEN-LAST:event_jrememberMouseClicked

    private void jPanelUPAncestorMoved(javax.swing.event.AncestorEvent evt) {//GEN-FIRST:event_jPanelUPAncestorMoved
        setPoint = super.getLocation();
    }//GEN-LAST:event_jPanelUPAncestorMoved

    private void check_rememberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_check_rememberActionPerformed
        if (evt.getSource() == check_remember) {
            if (check_remember.isSelected()) {
                setRememberMe();
            } else {
                setRememberMe();
            }
        }
    }//GEN-LAST:event_check_rememberActionPerformed

    private void setRememberMe() {

        String path = System.getProperty("user.dir") + "\\cache.log";
        
        if (check_remember.isSelected()) {
            try (BufferedWriter out = new BufferedWriter(new FileWriter(path));) {
                if (!name_txt.getText().isEmpty()) {
                    out.write(name_txt.getText().trim());
                    out.newLine();
                }
                if (!email_txt.getText().isEmpty()) {
                    out.write(email_txt.getText().trim());
                }
                out.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            File file = new File(path);
            file.delete();
        }
    }

    private ArrayList<String> getRememberMe() {

        ArrayList<String> rem = new ArrayList<>();
        String path = System.getProperty("user.dir") + "\\cache.log";
        try (BufferedReader in = new BufferedReader(new FileReader(path));) {

            String line;
            while ((line = in.readLine()) != null) {
                rem.add(line);
            }
            in.close();
        } catch (Exception ex) {
            setRememberMe();
        }
        return rem;
    }

    private void checkRememberMe() {
        ArrayList<String> list = getRememberMe();

        if (!list.isEmpty()) {
            name_txt.setText(list.get(0));
            email_txt.setText(list.get(1));
            check_remember.setSelected(true);
        }
    }

    private void closeWindow() {

        super.addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent windowEvent) {
                ImageIcon icon = new ImageIcon(getClass().getResource(config.getIconPath()));
                if (JOptionPane.showConfirmDialog(
                        windowEvent.getWindow(),
                        bundle.getString("key_msg_close"), bundle.getString("key_msg_close_title"),
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE,
                        icon
                ) == JOptionPane.YES_OPTION) {
                    System.exit(0);
                }
            }
        });
    }

    private void checkLocation(Point location) {
        setPoint = location;
        if (setPoint == null) {
            super.setLocationRelativeTo(null);
        } else {
            super.setLocation(setPoint);
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        try {
            /* Set the Nimbus look and feel */
            //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
            /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
            * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
             */
            try {
                for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                    if ("Nimbus".equals(info.getName())) {
                        javax.swing.UIManager.setLookAndFeel(info.getClassName());
                        break;
                    }
                }
            } catch (ClassNotFoundException ex) {
                java.util.logging.Logger.getLogger(login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            } catch (InstantiationException ex) {
                java.util.logging.Logger.getLogger(login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex) {
                java.util.logging.Logger.getLogger(login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            } catch (javax.swing.UnsupportedLookAndFeelException ex) {
                java.util.logging.Logger.getLogger(login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            }
            //</editor-fold>

            /* Create and display the form */
            javax.swing.UIManager.setLookAndFeel(new SyntheticaBlackEyeLookAndFeel());
            java.awt.EventQueue.invokeLater(new Runnable() {
                @Override
                public void run() {
                    final Locale RO = new Locale("ro", "Romania");
                    final Locale EN = new Locale("en", "US");
                    ResourceBundle bundle = ResourceBundle.getBundle("crypto/helper/lang_en", EN);
                    new login(setPoint, bundle).setVisible(true);
                }
            });
        } catch (UnsupportedLookAndFeelException | ParseException ex) {
            Logger.getLogger(login.class.getName()).log(Level.SEVERE, null, ex);
        }
        //</editor-fold>
    }

    private static Point setPoint = null;
    private static String location_key;
    private final configApp config = new configApp();
    private static ResourceBundle bundle;
    private String lang;

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton back;
    private javax.swing.JCheckBox check_remember;
    private javax.swing.JTextField email_txt;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanelDOWN;
    private javax.swing.JPanel jPanelUP;
    private javax.swing.JLabel jemail;
    private javax.swing.JLabel jname;
    private javax.swing.JLabel jremember;
    private javax.swing.JLabel jsign;
    private javax.swing.JButton jsign_in;
    private javax.swing.JButton jsign_up;
    private javax.swing.JLabel key;
    private javax.swing.JButton key_store;
    private javax.swing.JTextField name_txt;
    // End of variables declaration//GEN-END:variables

    @Override
    public void check_store(String path) {
        location_key = path;
        String check_crt = location_key.substring(location_key.length() - 3);
        if (check_crt.equals("crt")) {
            key_store.setIcon(new javax.swing.ImageIcon(getClass().getResource("/crypto/images/simple_forms/id_verified.png")));
        } else {
            location_key = null;
            key_store.setIcon(new javax.swing.ImageIcon(getClass().getResource("/crypto/images/simple_forms/id_not_verified.png")));
        }
    }

    @Override
    public void reset_store() {
        key_store.setIcon(new javax.swing.ImageIcon(getClass().getResource("/crypto/images/simple_forms/inputs.png")));
    }
}
