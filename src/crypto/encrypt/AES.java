// * @author Catalin Glavan
package crypto.encrypt;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.MessageDigest;

import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import javax.security.cert.X509Certificate;

import javax.crypto.Cipher;
import javax.crypto.CipherOutputStream;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.security.cert.CertificateException;
import javax.security.cert.CertificateExpiredException;
import javax.security.cert.CertificateNotYetValidException;
import javax.xml.bind.DatatypeConverter;
import org.bouncycastle.util.Arrays;

public class AES {

    public static void main(String[] args) throws CertificateException {
        try {
            BufferedInputStream is = new BufferedInputStream(new FileInputStream("C:\\Users\\Catalin Glavan\\Desktop\\mesaj.enc"));
            BufferedOutputStream os = new BufferedOutputStream(new FileOutputStream("C:\\Users\\Catalin Glavan\\Desktop\\mesaj.txt"));

            X509Certificate cert;
            try (InputStream inStream = new FileInputStream("C:\\Users\\Catalin Glavan\\Desktop\\gabi.crt")) {
                cert = X509Certificate.getInstance(inStream);
                cert.checkValidity();
            }
            encryptDecrypt(false, is, os, "password", cert.getPublicKey());
        } catch (IOException | CertificateExpiredException | CertificateNotYetValidException e) {
            e.printStackTrace();
        }
    }

    private static void encryptDecrypt(Boolean encrypt_decrypt, InputStream in, OutputStream out, String password, PublicKey key) {
        try {
            byte[] key256 = getSHA256(key.getEncoded());
            System.out.println(java.util.Arrays.toString(key256));
            byte[] iv = getSHA1_16(password);

            SecretKeySpec keySpec = new SecretKeySpec(key256, "AES");
            IvParameterSpec IvSpec = new IvParameterSpec(iv);

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            if (encrypt_decrypt) {
                cipher.init(Cipher.ENCRYPT_MODE, keySpec, IvSpec);
            } else {
                cipher.init(Cipher.DECRYPT_MODE, keySpec, IvSpec);
            }
            out = new CipherOutputStream(out, cipher);
            int numRead = 0;
            byte[] buf = new byte[1024];
            while ((numRead = in.read(buf)) >= 0) {
                out.write(buf, 0, numRead);
            }
            out.close();

        } catch (Exception e) {
        }
    }

    private static byte[] getSHA256(byte[] input) {
        if (input != null) {
            try {
                MessageDigest hasher = MessageDigest.getInstance("SHA-256");
                return hasher.digest(input);
            } catch (NoSuchAlgorithmException ex) {
                ex.printStackTrace();
            }
        }
        return null;
    }

    private static byte[] getSHA1_16(String input) {
        byte[] sha1 = null;
        if (input != null) {
            try {
                MessageDigest hasher = MessageDigest.getInstance("SHA-1");
                sha1 = hasher.digest(input.getBytes());
                sha1 = Arrays.copyOfRange(sha1, 0, 16);
                return sha1;

            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
        }
        System.out.println(DatatypeConverter.printBase64Binary(sha1));
        return null;
    }

    private static byte[] getSHA1(String input) {
        byte[] sha1 = null;
        if (input != null) {
            try {
                MessageDigest hasher = MessageDigest.getInstance("SHA-1");
                sha1 = hasher.digest(input.getBytes());
                return sha1;

            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
        }
        System.out.println(DatatypeConverter.printBase64Binary(sha1));
        return null;
    }

    /*
	Every implementation of the Java platform is required to support the following standard Cipher transformations with the keysizes in parentheses:
	AES/CBC/NoPadding (128)
	AES/CBC/PKCS5Padding (128)
	AES/ECB/NoPadding (128)
	AES/ECB/PKCS5Padding (128)
	DES/CBC/NoPadding (56)
	DES/CBC/PKCS5Padding (56)
	DES/ECB/NoPadding (56)
	DES/ECB/PKCS5Padding (56)
	DESede/CBC/NoPadding (168)
	DESede/CBC/PKCS5Padding (168)
	DESede/ECB/NoPadding (168)
	DESede/ECB/PKCS5Padding (168)
	RSA/ECB/PKCS1Padding (1024, 2048)
	RSA/ECB/OAEPWithSHA-1AndMGF1Padding (1024, 2048)
	RSA/ECB/OAEPWithSHA-256AndMGF1Padding (1024, 2048)
     */
}
