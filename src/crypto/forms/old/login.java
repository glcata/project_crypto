/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crypto.forms.old;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import javax.swing.ImageIcon;

/**
 *
 * @author Catalin Glavan
 */
public class login extends javax.swing.JPanel {

    private Image image;
    private helper helper;

    /**
     * Creates new form login
     *
     * @param help
     */
    public login(helper help) {
        initComponents();
        this.setDoubleBuffered(true);
        this.setOpaque(false);
        this.image = new ImageIcon(getClass().getResource("/crypto/images/right_panel1.png")).getImage();
        this.helper = help;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        back = new javax.swing.JButton();
        minimize = new javax.swing.JButton();
        exit = new javax.swing.JButton();
        info = new javax.swing.JButton();
        nume = new crypto.forms.old.text_fields();
        nume_txt = new javax.swing.JTextField();
        prenume = new crypto.forms.old.text_fields();
        prenume_txt = new javax.swing.JTextField();
        email = new crypto.forms.old.text_fields();
        email_txt = new javax.swing.JTextField();

        back.setIcon(new javax.swing.ImageIcon(getClass().getResource("/crypto/images/back_off.png"))); // NOI18N
        back.setBorder(null);
        back.setContentAreaFilled(false);
        back.setFocusable(false);
        back.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                backMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                backMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                backMousePressed(evt);
            }
        });
        back.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backActionPerformed(evt);
            }
        });

        minimize.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        minimize.setIcon(new javax.swing.ImageIcon(getClass().getResource("/crypto/images/minimize_off.png"))); // NOI18N
        minimize.setBorder(null);
        minimize.setBorderPainted(false);
        minimize.setContentAreaFilled(false);
        minimize.setDefaultCapable(false);
        minimize.setFocusPainted(false);
        minimize.setFocusable(false);
        minimize.setRequestFocusEnabled(false);
        minimize.setVerifyInputWhenFocusTarget(false);
        minimize.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                minimizeMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                minimizeMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                minimizeMouseExited(evt);
            }
        });
        minimize.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                minimizeActionPerformed(evt);
            }
        });

        exit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/crypto/images/exit_off.png"))); // NOI18N
        exit.setBorder(null);
        exit.setBorderPainted(false);
        exit.setContentAreaFilled(false);
        exit.setFocusable(false);
        exit.setRequestFocusEnabled(false);
        exit.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                exitMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                exitMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                exitMousePressed(evt);
            }
        });
        exit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exitActionPerformed(evt);
            }
        });

        info.setIcon(new javax.swing.ImageIcon(getClass().getResource("/crypto/images/info_off.png"))); // NOI18N
        info.setBorder(null);
        info.setContentAreaFilled(false);
        info.setFocusable(false);
        info.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                infoMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                infoMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                infoMousePressed(evt);
            }
        });
        info.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                infoActionPerformed(evt);
            }
        });

        nume.setText("Username");
        nume.setButton(0);

        nume_txt.setFont(new java.awt.Font("Dialog", 1, 15)); // NOI18N
        nume_txt.setForeground(new java.awt.Color(255, 255, 255));
        nume_txt.setBorder(null);
        nume_txt.setOpaque(false);
        nume_txt.setPreferredSize(new java.awt.Dimension(0, 24));
        nume_txt.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                nume_txtFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                nume_txtFocusLost(evt);
            }
        });
        nume_txt.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                nume_txtMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                nume_txtMouseExited(evt);
            }
        });

        javax.swing.GroupLayout numeLayout = new javax.swing.GroupLayout(nume);
        nume.setLayout(numeLayout);
        numeLayout.setHorizontalGroup(
            numeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, numeLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(nume_txt, javax.swing.GroupLayout.DEFAULT_SIZE, 284, Short.MAX_VALUE)
                .addContainerGap())
        );
        numeLayout.setVerticalGroup(
            numeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, numeLayout.createSequentialGroup()
                .addContainerGap(24, Short.MAX_VALUE)
                .addComponent(nume_txt, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        prenume.setText("Email");
        prenume.setButton(0);

        prenume_txt.setFont(new java.awt.Font("Dialog", 1, 15)); // NOI18N
        prenume_txt.setForeground(new java.awt.Color(255, 255, 255));
        prenume_txt.setBorder(null);
        prenume_txt.setOpaque(false);
        prenume_txt.setPreferredSize(new java.awt.Dimension(0, 24));

        javax.swing.GroupLayout prenumeLayout = new javax.swing.GroupLayout(prenume);
        prenume.setLayout(prenumeLayout);
        prenumeLayout.setHorizontalGroup(
            prenumeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(prenumeLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(prenume_txt, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        prenumeLayout.setVerticalGroup(
            prenumeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, prenumeLayout.createSequentialGroup()
                .addContainerGap(24, Short.MAX_VALUE)
                .addComponent(prenume_txt, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        email.setText("Password");
        email.setButton(0);

        email_txt.setFont(new java.awt.Font("Dialog", 1, 15)); // NOI18N
        email_txt.setForeground(new java.awt.Color(255, 255, 255));
        email_txt.setBorder(null);
        email_txt.setOpaque(false);
        email_txt.setPreferredSize(new java.awt.Dimension(0, 24));

        javax.swing.GroupLayout emailLayout = new javax.swing.GroupLayout(email);
        email.setLayout(emailLayout);
        emailLayout.setHorizontalGroup(
            emailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(emailLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(email_txt, javax.swing.GroupLayout.DEFAULT_SIZE, 283, Short.MAX_VALUE)
                .addContainerGap())
        );
        emailLayout.setVerticalGroup(
            emailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, emailLayout.createSequentialGroup()
                .addContainerGap(24, Short.MAX_VALUE)
                .addComponent(email_txt, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(34, 34, 34)
                        .addComponent(back)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(info))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap(196, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(nume, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(prenume, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(email, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(minimize)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(exit)
                .addGap(69, 69, 69))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(info)
                    .addComponent(minimize)
                    .addComponent(exit)
                    .addComponent(back))
                .addGap(82, 82, 82)
                .addComponent(nume, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(prenume, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(email, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(213, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void backMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_backMouseEntered
        if (evt.getSource() == back) {
            back.setIcon(new javax.swing.ImageIcon(getClass().getResource("/crypto/images/back_on.png")));
        }
    }//GEN-LAST:event_backMouseEntered

    private void backMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_backMouseExited
        if (evt.getSource() == back) {
            back.setIcon(new javax.swing.ImageIcon(getClass().getResource("/crypto/images/back_off.png")));
        }
    }//GEN-LAST:event_backMouseExited

    private void backMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_backMousePressed
        if (evt.getSource() == back) {
            // DO THIS
        }
    }//GEN-LAST:event_backMousePressed

    private void backActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backActionPerformed
        if (evt.getSource() == back) {
            back.setIcon(new javax.swing.ImageIcon(getClass().getResource("/crypto/images/back_on.png")));
            helper.intro();
        }
    }//GEN-LAST:event_backActionPerformed

    private void minimizeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_minimizeMouseClicked
        if (evt.getSource() == minimize) {
            minimize.setIcon(new javax.swing.ImageIcon(getClass().getResource("/crypto/images/minimize_off.png")));
        }
    }//GEN-LAST:event_minimizeMouseClicked

    private void minimizeMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_minimizeMouseEntered
        if (evt.getSource() == minimize) {
            minimize.setIcon(new javax.swing.ImageIcon(getClass().getResource("/crypto/images/minimize_on.png")));
        }
    }//GEN-LAST:event_minimizeMouseEntered

    private void minimizeMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_minimizeMouseExited
        if (evt.getSource() == minimize) {
            minimize.setIcon(new javax.swing.ImageIcon(getClass().getResource("/crypto/images/minimize_off.png")));
        }
    }//GEN-LAST:event_minimizeMouseExited

    private void minimizeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_minimizeActionPerformed
        if (evt.getSource() == minimize) {
            minimize.setIcon(new javax.swing.ImageIcon(getClass().getResource("/crypto/images/minimize_on.png")));
            helper.state_minimize();
        }
    }//GEN-LAST:event_minimizeActionPerformed

    private void exitMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_exitMouseEntered
        if (evt.getSource() == exit) {
            exit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/crypto/images/exit_on.png")));
        }
    }//GEN-LAST:event_exitMouseEntered

    private void exitMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_exitMouseExited
        if (evt.getSource() == exit) {
            exit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/crypto/images/exit_off.png")));
        }
    }//GEN-LAST:event_exitMouseExited

    private void exitMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_exitMousePressed
        if (evt.getSource() == exit) {
            // DO THIS
        }
    }//GEN-LAST:event_exitMousePressed

    private void exitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exitActionPerformed
        if (evt.getSource() == exit) {
            exit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/crypto/images/exit_on.png")));
            System.exit(0);
        }
    }//GEN-LAST:event_exitActionPerformed

    private void infoMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_infoMouseEntered
        if (evt.getSource() == info) {
            info.setIcon(new javax.swing.ImageIcon(getClass().getResource("/crypto/images/info_on.png")));
        }
    }//GEN-LAST:event_infoMouseEntered

    private void infoMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_infoMouseExited
        if (evt.getSource() == info) {
            info.setIcon(new javax.swing.ImageIcon(getClass().getResource("/crypto/images/info_off.png")));
        }
    }//GEN-LAST:event_infoMouseExited

    private void infoMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_infoMousePressed
        if (evt.getSource() == info) {
            // DO THIS
        }
    }//GEN-LAST:event_infoMousePressed

    private void infoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_infoActionPerformed
        if (evt.getSource() == info) {
            info.setIcon(new javax.swing.ImageIcon(getClass().getResource("/crypto/images/info_on.png")));
            // DO THIS
        }
    }//GEN-LAST:event_infoActionPerformed

    private void nume_txtMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_nume_txtMouseEntered
        if (evt.getSource() == nume_txt && nume_txt.getText().isEmpty()) {
            nume.setButton(3);
            refreshFrame();
        }
    }//GEN-LAST:event_nume_txtMouseEntered

    private void nume_txtFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_nume_txtFocusGained
        if (evt.getSource() == nume_txt) {
            nume.setButton(1);
            refreshFrame();
        }
    }//GEN-LAST:event_nume_txtFocusGained

    private void nume_txtFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_nume_txtFocusLost
        if (evt.getSource() == nume_txt) {
            if (nume_txt.getText().isEmpty()) {
                nume.setButton(0);
                refreshFrame();
            } else {
                nume.setButton(2);
                refreshFrame();
            }
        }
    }//GEN-LAST:event_nume_txtFocusLost

    private void nume_txtMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_nume_txtMouseExited
        if (evt.getSource() == nume_txt && nume_txt.getText().isEmpty()) {
            nume.setButton(0);
            refreshFrame();
            super.requestFocusInWindow();
        }
    }//GEN-LAST:event_nume_txtMouseExited

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2D = (Graphics2D) g;
        g2D.drawImage(image, 0, 0, null);
    }

    public void refreshFrame() {
        super.invalidate();
        super.validate();
        super.repaint();
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton back;
    private crypto.forms.old.text_fields email;
    private javax.swing.JTextField email_txt;
    private javax.swing.JButton exit;
    private javax.swing.JButton info;
    private javax.swing.JButton minimize;
    private crypto.forms.old.text_fields nume;
    private javax.swing.JTextField nume_txt;
    private crypto.forms.old.text_fields prenume;
    private javax.swing.JTextField prenume_txt;
    // End of variables declaration//GEN-END:variables
}
